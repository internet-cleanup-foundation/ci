FROM python:3.10

# install common dependencies
RUN pip install tox pipenv poetry pytest pytest-logging retry uv

# gdal-bin required for django.contrib.gis
RUN apt-get update -qq && \
  apt-get install -yqq gdal-bin make bash default-mysql-client && \
  rm -rf /var/lib/apt/lists/*

RUN curl -s https://storage.googleapis.com/shellcheck/shellcheck-stable.linux.x86_64.tar.xz | tar -C /usr/local/bin -Jxf -
#RUN chmod a+x /usr/local/bin/shellcheck
